#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#define __NR_xconcat	349	/* our private syscall number */
#define __user
int main(int argc, char *argv[])
{
	/*
	 *Input structure for passing arguments to xconcat system call
	 */
	struct ip{
		__user const char *outfile; // name of output file
		__user const char **infiles; // array with names of input files
		unsigned int infile_count; // number of input files in infiles array
		int oflags; // Open flags to change behavior of syscall
		mode_t mode; // default permission mode for newly created outfile
		unsigned int flags; // special flags to change behavior of syscall
	} input;
	int argcount = 0;
	int aflag = 0;
	int cflag = 0;
	int tflag = 0;
	int eflag = 0;
	int flag = 0;
	int Aflag = 0;
	int Nflag = 0;
	int Pflag = 0;
	int mvalue = 0644;
	int hflag = 0;
	char usage[] = "./xhw1 [-acte] [-m default_mode] [-NPA] outfile infile1 infile2 ...";

	/*
	 *Processing the command line arugments
	 */
	while ((argcount = getopt (argc, argv, "acteANPhm:")) != -1)
		switch(argcount)
		{
			case 'a':
				aflag = O_APPEND;
				break;
			case 'c':
				cflag = O_CREAT;
				break;
			case 't':
				tflag = O_TRUNC;
				break;
			case 'e':
				eflag = O_EXCL;
				break;
			case 'A':
				Aflag = 4;
				break;
			case 'N':
				Nflag = 1;
				break;
			case 'P':
				Pflag = 2;
				break;
			case 'h':
				hflag = 1;
				break;
			case '?':
				printf("Usage: %s\n For detailed usage use ./xhw1 -h\n", usage);
				return -1;
			case 'm':
				mvalue = strtol(optarg, NULL, 8);
			default:
				break;
		}
 	if (hflag){
		printf("Usage:\n%s\n",usage);
		printf("-c\tCreate outfile if it does not exist.\n"
			"-a\tAppends the concatinated data to outfile if it already exists.\n"
			"\tFails if the outfile does not exist.\n"
			"-t\tTruncates outfile before writing concatinated data to outfile.\n"
			"\tFails if the outfile does not exist.\n"
			"-m\tThe octal mode in which the outfile should be opened.\n"
			"\tDefault value is rwx for the user(700) creating the outfile.\n"
			"\tDoes nothing when outfile is already present.\n"
			"-N\treturns number of files instead of number of number of bytes written.\n"
			"-P\treturns percentage of bytes total bytes"
			"written instead of number of bytes written\n."
			"-A\tDoes not return  any partial writes. "
			"Returns total number of bytes written if the concatination is successful.\n\t"
			"Else, returns appropriate error and reverts the outfile to previous state.\n");
		return 0;
	} else if (optind < argc-1) {
  		input.outfile = argv[optind];
		input.infile_count = argc-(optind+1);
		input.infiles = malloc(sizeof(char*)*input.infile_count);
		int iterator = 0;
		for(iterator = 0; iterator < input.infile_count; iterator++){
			input.infiles[iterator] = argv[iterator + (optind+1)];	
		}
		input.oflags = aflag | tflag | cflag | eflag;
		if(mvalue) 
			input.mode = mvalue;
		if(Nflag == 1 && Pflag == 2){
			printf("N and P flags cannot be passed together\n. Please see Usage: \n%s\n",usage);
			return -1;
		}
  		input.flags = flag|Pflag|Nflag|Aflag; 
	} else {
		printf("No input files found. Please see Usage:\n%s\n", usage);

		return -1;
	}
	/*
	 *Passing the input structure to xconcat system call
	 */
	int rc;
	void *args = (void *) &input;
	int input_size = sizeof(input);
 	rc = syscall(__NR_xconcat, args, input_size);
	if (rc >  0)
		printf("syscall returned %d\n", rc);
	else
		printf("syscall returned %d (errno=%d)\n", rc, errno);

	return rc;
}
