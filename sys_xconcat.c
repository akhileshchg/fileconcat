#include <linux/fs.h>
#include <asm/segment.h>
#include <linux/uaccess.h>
#include <linux/buffer_head.h>
#include <linux/linkage.h>
#include <linux/moduleloader.h>
#include <linux/slab.h>
#include "sys_xconcat.h"
#define PAGESIZE 4096
asmlinkage extern long (*sysptr)(void *arg, int size);
/*
 *xconcat - concatinates files in kernel space
 *@arg :Arugments packed under one void *pointer
 *	Contains
 *	@outfile : output file to which concat result is written to
 *	@infiles : Array of input files
 *	@infile_count : Number of input files
 *	@oflags  : Openflags for output file. Similar to open system call
 *	@mode    : Open mode for output file. Similar to open system call
 *	@flags   : Modifies out result on setting this variable.
 *			Valid values are 0, 1, 2, and 4.
 *			0:	On success,returns num of bytes written to out
 *				On failure, could return error or num of bytes
 *				written till then
 *			1:	Same as 0. But returns number of files written
 *				instead ofnumber of bytes written.
 *			2:	Same as 0. But returns percentage of total
 *				bytes written instead of number of bytes writes
 *			4:	Atomic mode: returns number of bytes written on
 *				sucess.Reverts output file to original state
 *				state and returns appropriate error code.
 *@size : Size of the memory referenced by arg
 */
asmlinkage long xconcat(void *arg, int size)
{
	int ret = -EINVAL;
	struct ip *input;
	char *outfile = NULL;
	struct file *outfp = NULL;
	struct file **infp = NULL;
	char **infiles = NULL;
	int infile_count = 0;
	int i = 0;
	struct kstat ks;
	int totalsize = 0;
	mm_segment_t oldfs;
	void *buf = NULL;
	int page = PAGESIZE;
	int wb = 0;
	int rb = 0;
	int totalwrites = 0;
	int of = 0;
	int pos = 0;
	#ifdef EXTRA_CREDIT
	struct file *tmpfp = NULL;
	bool change = false;
	bool newfile = false;
	struct inode *parent = NULL;
	struct dentry *de = NULL;
	int check = 0;
	#endif
	input = NULL;
	/*
	 *Initial setup for input and out structures before concat
	 *1. Validates the memory received from userspace.
	 *2. Validates all the arugments packed inside arg pointer.
	 *3. Limits number of input files to 10.
	 *4. Checks if output file matches with any of the input files.
	 */
	if (arg == NULL) {
		printk(KERN_DEBUG "NULL passed as argument\n");
		return -EINVAL;
	}
	if (size != sizeof(struct ip))
		goto out;
	input = kmalloc(sizeof(struct ip), GFP_KERNEL);
	if (!input) {
		ret = -ENOMEM;
		goto out;
	}
	if (copy_from_user(input, arg, size)) {
		printk(KERN_ERR "Warning: Illegal memory address from user\n");
		ret = -EFAULT;
		goto out;
	}
	/*Checks if number of input files are more than 10*/
	infile_count = input->infile_count;
	if (infile_count <= 0 || infile_count > 10) {
		printk(KERN_ERR "Invalid number of files. "
			"Valid values should be between 1 and 10\n");
		if (infile_count > 10)
			ret = -E2BIG;
		goto out;
	}
	if (!(input->flags == C_BYTES  || input->flags == C_FILES ||
		input->flags == C_PCENT || input->flags == C_ATMIC ||
		input->flags == (C_FILES|C_ATMIC) ||
		input->flags == (C_PCENT|C_ATMIC))) {
		printk(KERN_ERR "Invalid return flag given."
			"Valid flags are %d, %d, %d, %d, %d, %d\n",
			C_BYTES, C_FILES, C_PCENT, C_ATMIC,
			C_FILES|C_ATMIC, C_PCENT|C_ATMIC);
		goto out;
	}
	#ifndef EXTRA_CREDIT
	if ((input->flags & C_ATMIC) == C_ATMIC)
		printk(KERN_INFO "xconcat: Not compiled with atomic support. "
			"Operation will go ahead in normal mode.\n"
			"Please recompile with EXTRA_CREDIT macro for atomic support\n");
	#endif
	outfile = getname(input->outfile);
	if (IS_ERR(outfile)) {
		printk(KERN_ERR "Invalid output file.\n");
		ret = PTR_ERR(outfile);
		goto out;
	}
	if ((input->oflags & O_TRUNC) == O_TRUNC)
		of = (input->oflags^O_TRUNC)|O_RDWR;
	else
		of = input->oflags|O_RDWR;
	#ifdef EXTRA_CREDIT
	if (((input->flags & C_ATMIC) == C_ATMIC) &&
		((input->oflags & O_CREAT) == O_CREAT)) {
		ret = openfile(&outfp, input->outfile, of|O_EXCL, input->mode);
		if (!ret) {
			newfile = true;
			closefile(&outfp);
			if ((of & O_EXCL) == O_EXCL)
				of = of^O_EXCL;
		} else if (ret != -EEXIST) {
			printk(KERN_ERR "Error opening output file\n");
			goto out;
		}
	}
	#endif
	ret = openfile(&outfp, input->outfile, of, input->mode);
	if (ret) {
		printk(KERN_ERR "Error opening output file\n");
		goto out;
	}
	infiles = kmalloc(sizeof(char *) * infile_count, GFP_KERNEL);
	if (!infiles) {
		ret = -ENOMEM;
		goto out;
	}
	if (copy_from_user(infiles, input->infiles,
		sizeof(char *) * infile_count)) {
		printk(KERN_ERR "Warning: Illegal memory address from user\n");
		ret = -EFAULT;
		goto out;
	}
	for (i = 0; i < infile_count; i++)
		infiles[i] = NULL;
	for (i = 0; i < infile_count; i++) {
		infiles[i] = getname(input->infiles[i]);
		if (IS_ERR(infiles[i])) {
			printk(KERN_ERR "One or more invalid input file(s)\n");
			ret = PTR_ERR(infiles[i]);
			goto out;
		}
	}
	infp = kmalloc(sizeof(struct file *), GFP_KERNEL);
	if (!infp) {
		ret = -ENOMEM;
		goto out;
	}
	for (i = 0; i < infile_count; i++)
		infp[i] = NULL;
	for (i = 0; i < infile_count; i++) {
		ret = openfile(&infp[i], infiles[i], O_RDONLY, 0);
		if (ret) {
			printk(KERN_ERR "Error opening one/more input files\n");
			goto out;
		} else if (((input->flags & C_PCENT) == C_PCENT)) {
			oldfs = get_fs();
			set_fs(KERNEL_DS);
			ret = vfs_stat(infiles[i], &ks);
			set_fs(oldfs);
			totalsize += (int)ks.size;
		}
	}
	/*
	 *Check if outfile matches with any of the infiles
	 */
	for (i = 0; i < infile_count; i++) {
		if (outfp->f_dentry->d_inode->i_ino
			== infp[i]->f_dentry->d_inode->i_ino) {
			printk(KERN_ERR "Error: Output file matches with"
				" one or more input files.\n");
			ret = -EINVAL;
			goto out;
		}
	}
	buf = kmalloc(page, GFP_KERNEL);
	if (!buf) {
		ret = -ENOMEM;
		goto out;
	}
	/*
	 *Atomic Setup: flag needs to be set to 4.
	 *1. Creates temp file.
	 *2. Copy contents if O_TRUNC is not given in oflags.
	 *3. Makes sure meta data of temp file and output are same.
	 *4. Writes concat result into temp file.
	 */
	#ifdef EXTRA_CREDIT
	if ((input->flags & C_ATMIC) == C_ATMIC) {
		printk(KERN_INFO "Atomic Mode: Creating tmp file\n");
		ret = openfile(&tmpfp, ".temp" ,
			O_CREAT|O_RDWR|input->oflags,
			outfp->f_dentry->d_inode->i_mode);
		if (ret)
			goto out;
		tmpfp->f_dentry->d_inode->i_uid
			= outfp->f_dentry->d_inode->i_uid;
		tmpfp->f_dentry->d_inode->i_gid
			= outfp->f_dentry->d_inode->i_gid;
		if (!((input->oflags & O_TRUNC) == O_TRUNC)) {
			outfp->f_pos = 0;
			do {
				rb = readfile(&outfp, buf, page);
				if (rb < 0) {
					ret = rb;
					printk(KERN_ERR "Atomic Mode: "
						"Read from out file failed\n");
					goto out;
				}
write:
				pos = tmpfp->f_pos;
				wb = writefile(&tmpfp, buf, rb);
				if (wb < 0) {
					ret = wb;
					printk(KERN_ERR "Atomic Mode: "
						"Write to tmp file failed\n");
					goto out;
				} else if (wb < rb) {
					tmpfp->f_pos = pos;
					goto write;
				}
			} while (rb > 0);
			if (!((input->oflags & O_APPEND) == O_APPEND))
				tmpfp->f_pos = 0;
		}
		swap_file(&outfp, &tmpfp);
		change = true;
	} else if ((input->oflags & O_TRUNC) == O_TRUNC) {
		closefile(&outfp);
		if ((input->oflags & O_CREAT) == O_CREAT
			&& (input->oflags & O_EXCL) == O_EXCL)
			of = input->oflags^O_EXCL;
		else
			of = input->oflags;
		ret = openfile(&outfp, input->outfile,
			of|O_RDWR, input->mode);
		if (ret)
			goto out;
	}
	#else
	if ((input->oflags & O_TRUNC) == O_TRUNC) {
		closefile(&outfp);
		if ((input->oflags & O_CREAT) == O_CREAT
			&& (input->oflags & O_EXCL) == O_EXCL)
			of = input->oflags^O_EXCL;
		else
			of = input->oflags;
		ret = openfile(&outfp, input->outfile,
			of|O_RDWR, input->mode);
		if (ret)
			goto out;
	}
	#endif
	/*
	 *Reads from input files and writes to output files
	 */
	for (i = 0; i < infile_count; i++) {
		do {
			rb = readfile(&infp[i], buf, page);
			if (rb > 0) {
rewrite:
				pos = outfp->f_pos;
				wb = writefile(&outfp, buf, rb);
				if (wb < 0) {
					#ifdef EXTRA_CREDIT
					if ((input->flags & C_ATMIC)
						== C_ATMIC)
						totalwrites = wb;
					#endif
					ret = totalwrites;
					printk(KERN_ERR "Write to"
						"output file failed\n");
					goto out;
				}
				if (wb < rb) {
					printk(KERN_INFO "Partial write"
						"encountered.\n"
						"Trying to rewrite\n");
					outfp->f_pos = pos;
					goto rewrite;
				}
				if ((input->flags & C_FILES) != C_FILES)
					totalwrites += wb;
			} else if (rb < 0) {
				#ifdef EXTRA_CREDIT
				if ((input->flags & C_ATMIC) == C_ATMIC)
					totalwrites = rb;
				#endif
				ret = totalwrites;
				printk(KERN_ERR "Read from one or more"
					"input files failed\n");
				goto out;
			}
		} while (rb > 0);
		if ((input->flags & C_FILES) == C_FILES)
			totalwrites++;
	}
	ret = totalwrites;

out:
	/*
	*Atomic Cleanup
	*1. On success, unlink output, rename temp file to output.
	*2. On failure, unlink temp file.
	*/
	#ifdef EXTRA_CREDIT
	if (input && (input->flags & C_ATMIC) == C_ATMIC) {
		printk(KERN_INFO "Atomic Mode: Clean up started\n");
		if (ret < 0 && change) {
			printk(KERN_ERR "Concat failed."
				"Reverting output to previous state\n");
			swap_file(&outfp, &tmpfp);
		}
		if (tmpfp) {
			parent = tmpfp->f_dentry->d_parent->d_inode;
			de = tmpfp->f_dentry;
			closefile(&tmpfp);
			check = vfs_unlink(parent, de);
			if (check) {
				printk(KERN_ERR
					"Error: Atomic clean up failed"
					". Check for "
					"filesystem issues.\n");
				ret = check;
			}
		}
		if (ret < 0 && newfile) {
			if (outfp) {
				parent = outfp->f_dentry->d_parent->d_inode;
				de = outfp->f_dentry;
				closefile(&outfp);
				check = vfs_unlink(parent, de);
				if (check) {
					printk(KERN_ERR
						"Error: Atomic clean up failed"
						". Check for filesystem "
						"consistency issues.\n");
					ret = check;
				}
			}
		} else if (ret > 0) {
			if (outfp && parent && de) {
				check = vfs_rename(outfp->f_dentry->
						d_parent->d_inode,
						outfp->f_dentry, parent, de);
				if (check) {
					printk(KERN_ERR
						"Error: Atomic clean up"
						"failed. Chcek for file"
						"system issues.\n");
					ret = check;
				}
			}
		}
	}
	#endif
	/*
	*Clean up
	*Free all the kernel structures used to store contents of arg
	*/
	if (ret >= 0 && (input->flags & C_PCENT) == C_PCENT) {
		if (totalsize)
			ret = ret*100/totalsize;
		else
			ret = 100;
	}
	kfree(buf);
	if (infp) {
		for (i = 0; i < infile_count; i++) {
			if (infp[i])
				closefile(&infp[i]);
		}
		kfree(infp);
	}
	if (outfp)
		closefile(&outfp);
	if (infiles) {
		for (i = 0; i < infile_count; i++) {
			if (infiles[i])
				putname(infiles[i]);
		}
		kfree(infiles);
	}
	if (outfile)
		putname(outfile);
	kfree(input);
	return ret;

}
static int __init init_sys_xconcat(void)
{
	printk(KERN_INFO "installed new sys_xconcat module\n");
	if (sysptr == NULL)
		sysptr = xconcat;
	return 0;
}
static void  __exit exit_sys_xconcat(void)
{
	if (sysptr != NULL)
		sysptr = NULL;
	printk(KERN_INFO "removed sys_xconcat module\n");
}
module_init(init_sys_xconcat);
module_exit(exit_sys_xconcat);
MODULE_LICENSE("GPL");
